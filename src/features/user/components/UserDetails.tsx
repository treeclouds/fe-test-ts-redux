import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Box,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  TextField,
  Typography
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import AddIcon from "@mui/icons-material/Add";
import Button from "@mui/material/Button";
import { FC, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { selectUsers } from "../UserSelectors";
import { actions } from "../UserSlice";


const UserDetails: FC = () => {
  const dispatch = useDispatch();
  const users = useSelector(selectUsers);
  const [showModal, toggleModal] = useState(false);
  const [name, setName] = useState("false");

  useEffect(() => {
    dispatch(actions.loadUsers())
  }, [dispatch]);

  const handleDelete = (id: number): any => {
    dispatch(actions.deleteUser(id))
  };

  const onChange = (e: React.FormEvent) => {
    var unsafeSearchTypeValue = ((e.target) as any).value;
    setName(unsafeSearchTypeValue);
  }

  const handleClickOpen = () => {
    toggleModal(true);
  };

  const handleClose = () => {
    toggleModal(false);
  };

  const handleAdd = () => {
    dispatch(actions.addUser({ name: name }));
    handleClose();
  }

  const onKeyPress = (ev:any) => {
    if (ev.key === 'Enter') {
      handleAdd();
    }
  }

  return (
    <Box style={{ padding: "5%" }}>
      <Typography variant="h4" component="h4"> Fe Test TS Redux </Typography>
      <Box display="flex" justifyContent="right" mb={2}>
        <Button
          onClick={handleClickOpen}
          variant="contained"
          startIcon={<AddIcon />}
        >
          Add
        </Button>
      </Box>

      <Dialog open={showModal} onClose={handleClose}
        fullWidth maxWidth="sm">
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          Add User
        </DialogTitle>
        <DialogContent>
          <TextField
            onChange={(e) => onChange(e)}
            onKeyPress={(ev) => onKeyPress(ev)}
            id="standard-basic" label="Name" variant="standard" fullWidth />
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose}>
            Cancel
          </Button>
          <Button onClick={handleAdd} variant="contained">Add</Button>
        </DialogActions>
      </Dialog>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }}>
          <TableHead>
            <TableRow>
              <TableCell align="center">ID</TableCell>
              <TableCell align="center">Name</TableCell>
              <TableCell align="center">Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {users.map((user) => (
              <TableRow
                key={user.id}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell align="center">{user.id}</TableCell>
                <TableCell align="center">{user.name}</TableCell>
                <TableCell align="center">
                  <Button
                    onClick={() => handleDelete(user.id)}
                    variant="outlined"
                    color="warning"
                    startIcon={<DeleteIcon />}
                  >
                    Delete
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  )
}

export default UserDetails;

