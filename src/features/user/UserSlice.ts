import { ActionReducerMapBuilder, createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { UserModel, UserState } from "./UserModel";

const initialState: UserState = {
  users: []
}

const loadUsers = createAsyncThunk(
  "users/loadUsers",
  async (_, { rejectWithValue }) => {
    try {
      const response = await axios.get("http://localhost:3010/users");
      return response.data;
    }
    catch(ex: any) {
      return rejectWithValue(ex.message);
    }
  }
);

const addUser = createAsyncThunk(
  "users/addUser",
  async (user: any, { rejectWithValue }) => {
    try {
      const response = await axios.post("http://localhost:3010/users", user);
      return response.data;
    }
    catch(ex: any) {
      return rejectWithValue(ex.message);
    }
  }
);

const deleteUser = createAsyncThunk(
  "users/deleteUser",
  async (id: number, { rejectWithValue }) => {
    try {
      await axios.delete(`http://localhost:3010/users/${id}`);
      return id;
    }
    catch(ex: any) {
      return rejectWithValue(ex.message);
    }
  }
);

interface ActionInterface {
  payload: any
}

const extraReducers = (builder: ActionReducerMapBuilder<UserState>) => {
  builder.addCase(loadUsers.fulfilled, (state, action:ActionInterface) => {
    state.users = action.payload
  })
  .addCase(addUser.fulfilled, (state, action:ActionInterface) => {
    state.users.push(action.payload)
  })
  .addCase(deleteUser.fulfilled, (state, action:ActionInterface) => {
    const data = state.users.filter((user:UserModel) => user.id !== action.payload)
    state.users = data;
  })
}

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: { },
  extraReducers
});

export const actions = { loadUsers, addUser, deleteUser, ...userSlice.actions };

export default userSlice.reducer;